analysis_begin='2016-01-01' # inclusive
analysis_end='2020-02-01' # exclusive

weights = {
        'guardian': 1,
    }

res = './out/res_all/'
assets = './out/assets_all/'
mods = './out/mod_all/'

pars = {
    'date_start': analysis_begin, 'date_end': analysis_end,
    'res_dir': res, 'assets_dir': assets, 'mod_dir': mods, 'weights': weights
}

text_column = 'text'
date_column = 'full_date'
name = 'guardian'
sources = list(weights.keys())
test_word = 'facebook'
min_count_value = 5
svd_dimensions = 250
perps = [300, 25]