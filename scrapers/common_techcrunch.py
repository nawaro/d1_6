import pandas as pd
import requests
import json
from bs4 import BeautifulSoup
import html2text
from common_scraping import *

art_dicts = []

def create_text_maker():
    text_maker = html2text.HTML2Text()
    text_maker.ignore_links = True
    text_maker.ignore_images = True
    text_maker.ignore_emphasis = True
    return text_maker

time_limit = pd.to_datetime('today') - delta()
print(time_limit)
time_limit_check = True
page = 1




while time_limit_check:
    response = requests.get(f'https://techcrunch.com/wp-json/tc/v1/magazine?page={page}&_embed=true')
    for elem in response.json():
        if pd.to_datetime(elem['date_gmt']) < time_limit:
                time_limit_check = False
                break 
#         print(elem['link'])
#         print(html2text.html2text(elem['title']['rendered']))
#         print([author['name'] for author in elem['_embedded']['authors']])
#         print(elem['date_gmt'])
        try:
            text_maker = create_text_maker()
            art_dicts.append({'link': elem['link'],
                              'authors': [author['name'] for author in elem['_embedded']['authors']],
                             'title': html2text.html2text(elem['title']['rendered']),
                             'date': elem['date_gmt'],
                             'texts': [text.replace('\n', ' ') for text in text_maker.handle(elem['content']['rendered']).split('\n\n')]})
        except:
            print('error')
            print(elem['link'])
            print(html2text.html2text(elem['title']['rendered']))
    print(f'Scraped page {page}', art_dicts[-1]['date'])
    page=page+1
    
    if page % 10 == 0:
        df_techcrunch = pd.DataFrame(art_dicts)
        df_techcrunch.to_csv(csv_dir_common() + 'techcrunch' + '.csv')

df_techcrunch = pd.DataFrame(art_dicts)
df_techcrunch.to_csv(csv_dir_common() + 'techcrunch' + '.csv')